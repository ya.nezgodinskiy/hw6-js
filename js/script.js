// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// Екранування це використання спеціальних символів для представлення спеціальних значень або зарезервованих символів в рядках або текстових даних. Потрібне щоб включати в рядки символи, які можуть інтерпретуватись як синтаксична конструкція.

// 2. Які засоби оголошення функцій ви знаєте?

// - function() {};
// - let functionName = function() {};
// - let functionName = () => {};

// 3. Що таке hoisting, як він працює для змінних та функцій?

// Це механізм, який дозволяє змінні та функції піднімати вгору перед виконанням коду. За допомогою hoisting можна викликати функції чи отримувати доступ до змінних, навіть якщо їх оголошення знаходиться після виклику чи використання.
// Змінна піднімається вгору, але її ініціалізація залишається на місці. Функція повністю піднімається вгору.


function createNewUser() {
  let newUser = {};
  let firstName = prompt("Ваше ім'я?");
  let secondName = prompt("Ваше прізвище?");
  let birthday = prompt("Дата народження?");

  newUser.firstName = firstName;
  newUser.secondName = secondName;
  newUser.birthday = birthday;

  newUser.getAge = function() {
    let today = new Date();
    let birthDate = new Date(birthday.split('.').reverse().join('-'));
    let age = today.getFullYear() - birthDate.getFullYear();
    let monthDiff = today.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  };

  newUser.getPassword = function() {
    let firstInitial = firstName.charAt(0).toUpperCase();
    let password = firstInitial + secondName.toLowerCase() + birthday.split('.').reverse().join('');
    return password;
  };

  return newUser;
}

let user = createNewUser();
console.log(user);
console.log("Age: " + user.getAge());
console.log("Password: " + user.getPassword());
